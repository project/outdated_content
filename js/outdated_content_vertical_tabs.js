/**
 * @file
 * Defines jQuery to provide summary information inside outdated content vertical tabs.
 */

(function ($) {

  'use strict';

  /**
   * Provide summary information for vertical tabs.
   */
  Drupal.behaviors.outdated_content_settings = {
    attach: function (context) {

      // Provide outdated content summary when editing a node.
      $('details#edit-outdated-settings', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-oc-override-value', context).is(':checked')) {
          var days = $('#edit-oc-override-days-0-value', context).val();
          if (days.length <= 0) {
            vals.push(Drupal.t('Enabled'));
          } else {
            vals.push(Drupal.t('Enabled (' + days + ' days)'));
          }
        }
        return vals.join('<br/>');
      });

      // Provide outdated content summary during content type configuration.
      $('#edit-outdated-content-config', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-enable-outdating', context).is(':checked')) {
          var days = $('#edit-outdated-days', context).val();
          if (days.length <= 0) {
            vals.push(Drupal.t('Enabled'));
          } else {
            vals.push(Drupal.t('Enabled (' + days + ' days)'));
          }
        }
        if ($('#edit-outdated-override', context).is(':checked')) {
          vals.push(Drupal.t('Overriding enabled'));
        }
        return vals.join('<br/>');
      });
    }
  };

})(jQuery);
