<?php

/**
 * @file
 * Contains supporting hooks that adjust the outdated content view.
 */

use Drupal\node\Entity\NodeType;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * Implements hook_views_pre_build().
 */
function outdated_content_views_pre_view(ViewExecutable $view, $display_id, array &$args) {
  if ($view->id() == 'outdated_content') {

    $outdated_content_service = \Drupal::service('outdated_content.status_check');

    // Getting a listing of content types using outdating.
    $content_types = $outdated_content_service->getOutdatedNodeTypes();

    // Get the first content type that's tagged as outdated.
    $first_outdated = $outdated_content_service->getFirstOutdatedType();

    // If a result is found, set the default content type to the selected type.
    if ($first_outdated) {
      // Change to a valid content type in the system.
      $view->display_handler->default_display->options['filters']['type']['value']['page'] = $first_outdated;
    }
    else {
      $view->display_handler->default_display->options['filters']['type']['value']['page'] = array_key_first($content_types);
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function outdated_content_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {

  // Alter the outdated content view query.
  if ($view->id() == 'outdated_content') {

    // Loading the outdated content configuration and storing the debug value.
    $config = \Drupal::config('outdated_content.config');
    $debugging = $config->get('oc_debug_mode');

    $outdated_content_service = \Drupal::service('outdated_content.status_check');

    // Getting a listing of content types using outdating.
    $content_types = $outdated_content_service->getOutdatedNodeTypes();

    // Grab the type ($_GET['type']) from the URL (if set).
    $get_type = \Drupal::request()->query->get('type');

    // Store the first content type.
    $current_type = array_key_first($content_types);

    // Switch $current_type to the first content type that
    // has outdating enabled.
    $first_outdated = $outdated_content_service->getFirstOutdatedType();

    if ($first_outdated) {
      $current_type = $first_outdated;
    }

    // If the type is defined in the URL, let's filter based on
    // that, but before we do, let's make sure it's a type of
    // content that exists in the system.
    if (isset($get_type) && array_key_exists($get_type, $content_types)) {
      // Updating the type.
      $current_type = $get_type;
    }

    // Traverse through the 'where' part of the query.
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {

        // If the current field value is the 'content type' filter.
        if ($condition['field'] == 'node_field_data.type') {

          $condition = [
            'field' => 'node_field_data.type',
            'operator' => 'in',
          ];

          // Setting the contents of the conditions array.
          $condition['value'] = [
            0 => $current_type,
          ];
        }

        // Load the bundle and get the value (if set) of the offset
        // for the given bundle.
        $node_type = NodeType::load($current_type);
        $outdated_offset = (int) $node_type->getThirdPartySetting('outdated_content', 'outdated_days');

        // If an outdated content offset is found, lets' configure the offset.
        if (isset($outdated_offset) && is_numeric($outdated_offset) && $outdated_offset > 0) {
          $outdated_days = $outdated_offset * 86400;
        }
        else {
          // If one isn't found, let's configure a manual offset to ensure
          // content doesn't suddenly show up - 100 years should do it!!
          $outdated_days = 3153600000;
        }

        // If the current field value is the 'changed date' filter.
        if ($condition['field'] == 'node_field_data.changed <= ***CURRENT_TIME***-86400') {

          // If debugging is enabled, let's change the $outdated_days
          // variable to zero (this essentially returns everything).
          if (isset($debugging) && $debugging) {
            $outdated_days = 0;
          }

          // Setting the contents of the conditions array.
          $condition = [
            'field' => 'node_field_data.changed <= ***CURRENT_TIME***-' . $outdated_days,
            'value' => [],
            'operator' => 'formula',
          ];
        }
      }
    }
  }
}
