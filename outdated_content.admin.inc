<?php

/**
 * @file
 * Administration form for the outdated content module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Helper function for the real hook_form_FORM_ID_alter().
 *
 * @see outdated_content_form_node_type_form_alter()
 */
function _outdated_content_form_node_type_form_alter(array &$form, FormStateInterface $form_state) {

  // Attaching oc-vertical-tabs JS.
  $form['#attached']['library'][] = 'outdated_content/oc-vertical-tabs';

  // Grab the 'outdated_content' configuration.
  $config = \Drupal::config('outdated_content.config');

  /** @var \Drupal\node\NodeTypeInterface $type */
  $type = $form_state->getFormObject()->getEntity();

  $form['outdated_content_config'] = [
    '#type' => 'details',
    '#title' => t('Outdated content'),
    '#weight' => 35,
    '#group' => 'additional_settings',
  ];

  $form['outdated_content_config']['enable_outdating'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable outdated content'),
    '#default_value' => $type->getThirdPartySetting('outdated_content', 'enable_outdating', $config->get('enable_outdating')),
  ];

  $form['outdated_content_config']['outdated_days'] = [
    '#type' => 'number',
    '#min' => 1,
    '#title' => t('Number of days'),
    '#description' => t(
      'Days after publication that @type nodes become outdated.', [
        '@type' => strtolower($type->label()),
      ],
    ),
    '#default_value' => $type->getThirdPartySetting('outdated_content', 'outdated_days', $config->get('outdated_days')),
    '#states' => [
      'visible' => [
        ':input[name="enable_outdating"]' => ['checked' => TRUE],
      ],
      'required' => [
        ':input[name="enable_outdating"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['outdated_content_config']['outdated_override'] = [
    '#type' => 'checkbox',
    '#min' => 1,
    '#title' => t('Enable overriding'),
    '#description' => t(
      'Allow overriding of the number of days, per item, for @type content.', [
        '@type' => strtolower($type->label()),
      ],
    ),
    '#default_value' => $type->getThirdPartySetting('outdated_content', 'outdated_override', $config->get('outdated_override')),
    '#states' => [
      'visible' => [
        ':input[name="enable_outdating"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['#entity_builders'][] = '_outdated_content_form_node_type_form_builder';
}

/**
 * Entity builder for the node type form with outdated content options.
 */
function _outdated_content_form_node_type_form_builder($entity_type, NodeTypeInterface $type, &$form, FormStateInterface $form_state) {
  $type->setThirdPartySetting('outdated_content', 'enable_outdating', $form_state->getValue('enable_outdating'));
  $type->setThirdPartySetting('outdated_content', 'outdated_days', $form_state->getValue('outdated_days'));
  $type->setThirdPartySetting('outdated_content', 'outdated_override', $form_state->getValue('outdated_override'));
}
