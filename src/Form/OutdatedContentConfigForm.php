<?php

namespace Drupal\outdated_content\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for outdated content.
 */
class OutdatedContentConfigForm extends ConfigFormBase {

  /**
   * Gets an array of role types.
   *
   * @var array
   */
  private array $userRoles;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->userRoles = \Drupal::entityTypeManager()
      ->getStorage('user_role')->loadMultiple();
  }

  /**
   * Outdated content config.
   *
   * @var string
   */
  const OUTDATED_CONTENT = 'outdated_content.config';

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'outdated_content_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::OUTDATED_CONTENT,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Load the oudated content configuration.
    $config = $this->config(static::OUTDATED_CONTENT);

    $form['oc_display_banner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display banner'),
      '#description' => $this->t('Displays a banner above all outdated content.'),
      '#default_value' => $config->get('oc_display_banner'),
    ];

    // Look through the user roles and add them to an array with
    // the id set as the key and the label set as the value.
    $roles = [];
    foreach ($this->userRoles as $role) {
      $roles[$role->id()] = $role->label();
    }

    $form['oc_banner_allowed_roles'] = [
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Restrict display of a banner to specific roles <em>(unrestricted by default)</em>.'),
      '#default_value' => $config->get('oc_banner_allowed_roles'),
      '#states' => [
        'visible' => [
          ':input[name="oc_display_banner"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['oc_debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('Sets the number of days that must pass before content is outdated to 0.'),
      '#default_value' => $config->get('oc_debug_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // Validation to go in here.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Load the oudated content configuration.
    $config = $this->config(static::OUTDATED_CONTENT);

    // If "display banner" is unchecked, ensure the value is cleared in
    // the config along with the value(s) for the set "roles".
    if (!$form_state->getValue('oc_display_banner')) {
      $config->clear('oc_display_banner');
      $config->clear('oc_banner_allowed_roles');
    }
    else {
      // Otherwise, set whatever is set in the $form_state for each.
      $config->set('oc_display_banner', $form_state->getValue('oc_display_banner'));
      $config->set('oc_banner_allowed_roles', $form_state->getValue('oc_banner_allowed_roles'));
    }

    // Save the value of the "enable debugging" field.
    $config->set('oc_debug_mode', $form_state->getValue('oc_debug_mode'));

    // Save the configuration.
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
