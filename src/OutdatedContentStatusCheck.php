<?php

namespace Drupal\outdated_content;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Check to see if the content we're viewing uses outdated content.
 */
class OutdatedContentStatusCheck {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs the OutdatedContentStatusCheck object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManager $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Work out how out of date the content is.
   *
   * @return array|false
   *   Outdated content information.
   */
  public function getOutDatedStatus() {

    $node = $this->routeMatch->getParameter('node');

    // First, let's make sure we're viewing a node.
    if ($node instanceof NodeInterface) {

      // If we are, load the bundle object along with the value of the
      // 'enable_outdating' setting.
      $node_type = $this->entityTypeManager->getStorage('node_type');
      $bundle = $node_type->load($node->bundle());
      $outdated_content = $bundle->getThirdPartySetting('outdated_content', 'enable_outdating');

      // If outdated content has been configured, lets determine
      // how out of date the content is.
      if ($outdated_content) {

        // Store the current time.
        $now = \Drupal::time()->getCurrentTime();

        // Store the node updated date.
        $node_updated = $node->getChangedTime();

        // Overriding values.
        $override = $node->get('oc_override')->value;
        $override_days = $node->get('oc_override_days')->value;
        $override_timestamp = $node->get('oc_override_timestamp')->value;

        // Check to see if debug mode is enabled.
        $config = \Drupal::config('outdated_content.config');
        $debugging = $config->get('oc_debug_mode');

        // 1. Load the value of 'outdated_days' for this type of content
        // 2. Check how much time has passed (in days) since last update
        // 3. Calculate the remaining days before the content becomes outdated
        // 4. Set a status of FALSE.
        if ($override) {
          $outdated_days = $override_days;
        }
        else {
          $outdated_days = $bundle->getThirdPartySetting('outdated_content', 'outdated_days');
        }

        $days_passed = ceil(abs(($now - $node_updated) / 86400));
        $remaining_days = $outdated_days - $days_passed;
        $status = FALSE;

        // Before setting the status to true, check whether:
        // 1. Debugging is enabled
        // 2. The remaining days is less than (or equal to) zero.
        if ($debugging || $remaining_days <= 0) {
          $status = TRUE;
        }

        // Return array.
        return [
          'outdated_days' => $outdated_days,
          'remaining_days' => $remaining_days,
          'type' => $bundle->label(),
          'status' => $status,
        ];
      }

    }
    elseif ($this->routeMatch->getRouteName() == 'view.outdated_content.outdated_content') {

      // Getting a listing of content types using outdating.
      $content_types = $this->getOutdatedNodeTypes();

      // Grab the type ($_GET['type']) from the URL (if set).
      $get_type = \Drupal::request()->query->get('type');

      // If the type is set in the URL, set the $id variable.
      if (isset($get_type) && array_key_exists($get_type, $content_types)) {
        $id = $get_type;
      }
      else {
        if ($this->getFirstOutdatedType()) {
          // Get the id of the first content type with outdated content set.
          $id = $this->getFirstOutdatedType();
        }
        else {
          // If no content is using outdated conte, simply set the id as
          // the first content type in the system.
          $id = array_key_first($content_types);
        }
      }

      // Load the bundle data for the content type.
      $bundle = $this->entityTypeManager
        ->getStorage('node_type')
        ->load($id);

      $outdating_enabled = $bundle->getThirdPartySetting('outdated_content', 'enable_outdating');
      $outdated_offset = $bundle->getThirdPartySetting('outdated_content', 'outdated_days');

      // If outdated content isn't enabled (this should not fire), show
      // a generic message.
      if (isset($outdating_enabled) && !$outdating_enabled) {
        \Drupal::messenger()->addMessage(
          t('Outdated content is not enabled for @type content.',
            [
              '@type' => strtolower($content_types[$id]['label']),
            ]
          )
        );
      }
      else {
        // Otherwise, give the user some information relevant to the
        // content type we're currently filtering against.
        \Drupal::messenger()->addMessage(
          t('Showing @type content last updated more than @days days ago.',
            [
              '@type' => strtolower($content_types[$id]['label']),
              '@days' => $outdated_offset,
            ]
          )
        );
      }
      return FALSE;
    }
    // If we're not viewing a node, simply return FALSE.
    return FALSE;
  }

  /**
   * Determine which content types are using outdated content.
   *
   * @return array|false
   *   An array of content types with outdated content enabled.
   */
  public function getOutdatedNodeTypes() {
    // Load an array of the various node types in the system.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Determine which ones are out of date (return everything for now).
    $content_types = [];
    foreach ($node_types as $node_type) {
      $outdating = $this->entityTypeManager
        ->getStorage('node_type')
        ->load($node_type->id())
        ->getThirdPartySetting('outdated_content', 'enable_outdating');

      $content_types[$node_type->id()] = [
        'label' => $node_type->label(),
        'outdating' => $outdating,
      ];
    }
    return $content_types;
  }

  /**
   * Checks which content type (alphabetically) has outdating enabled.
   *
   * @return string|false
   *   The first content type with outdating enabled.
   */
  public function getFirstOutdatedType() {
    // Getting a listing of content types using outdating.
    $content_types = $this->getOutdatedNodeTypes();
    $current_type = FALSE;

    foreach ($content_types as $key => $val) {
      if ($val['outdating']) {
        $current_type = $key;
        break;
      }
    }
    return $current_type;
  }

}
